// Module Export

interface Mobile {
  id: number;
  color: string;
  type: string;
}

const PRICE = 1000;

export { Mobile, PRICE };

// Alternatives, not recommended
// export default Mobile;
// export default PRICE;
