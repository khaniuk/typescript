// Interface

// Time compilation
// Check types

interface Person {
  name: string;
  age?: number;
}

const person: Person = {
  name: "Rick",
  age: 35,
};
console.log(person);

// -------------------------------

const persons: Person[] = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
];
console.log(persons);

function getFirst(position: number): Person {
  return persons[position];
}
console.log(getFirst(1));

// -------------------------------

interface User extends Person {
  phone?: number;
  admin: boolean;
  isAdmin?: (admin: boolean) => string;
}

const user: User = {
  name: "Judith",
  age: 10,
  admin: true,
  isAdmin: (admin: boolean) => {
    return admin ? "admin" : "don't admin";
  },
};

console.log(user);
