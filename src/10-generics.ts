// Generics

// Reusable code
interface Values {
  x: number;
  y: number;
}

class Operator<T extends { x: number; y: number }> {
  sum(values: T) {
    return values.x + values.y;
  }
}

const operator = new Operator<Values>();

const result = operator.sum({ x: 2, y: 3 });
console.log("Sum is", result);
