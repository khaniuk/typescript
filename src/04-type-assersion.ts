// Type Assersion

let nameInit: any = "Rick";

let firstname = <string>nameInit;

console.log(firstname);

firstname = nameInit as string;

console.log(firstname);
