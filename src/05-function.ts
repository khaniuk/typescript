// Functions

function hello(world: string): void {
  console.log(`Hello ${world}`);
}

hello("world");

// ------------------------------

function sum(x: number, y: number): number {
  return x + y;
}

console.log("Sum", sum(2, 3));

// ------------------------------

function substraction(values: { x: number; y: number }): number {
  return values.x - values.y;
}

let params = { x: 4, y: 1 };
console.log("Substraction", substraction(params));

// ------------------------------

function multiplication(values: { x: number; y: number; z?: number }): number {
  return values.x * values.y * (values.z ? values.z : 1);
}

params = { x: 2, y: 3 };
console.log("Multiplication", multiplication(params));

const objParams = { x: 2, y: 3, z: 4 };
console.log("Multiplication", multiplication(objParams));

// ------------------------------

function division(x: number, y: number = 1): number {
  return x / y;
}

console.log("Division", division(10, 2));
