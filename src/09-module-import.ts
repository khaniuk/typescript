import { Mobile } from "./08-module-export";

// Module import
const phone: Mobile = {
  id: 10,
  color: "White",
  type: "Phone",
};
console.log("Mobile", phone);

// Alternatives, not recommended
import { PRICE as myPrice } from "./08-module-export";
console.log(`Value PRICE ${myPrice}`);

// import * as importData from "./08-module-export";
// console.log(`Rename PRICE with as ${importData.PRICE}`);
