// Types

let typeString: string = "Hello world";
console.log(`String: ${typeString}`);

let typeNumber: number = 20;
console.log(`Number: ${typeNumber}`);

let typeBoolean: boolean = true;
console.log(`Boolean: ${typeBoolean}`);

let typeAny: any = 1000000000000;
console.log(`Any: ${typeAny}`);

// Arrays
let arrayNumber1: number[] = [1, 2, 3, 4];
console.log(`Array numbers 1: ${arrayNumber1}`);

let arrayNumber2: Array<number> = [5, 6, 7, 8];
console.log(`Array numbers 2: ${arrayNumber2}`);

// let arrayNumber3: number[] = [1, 2, 3, 4, "One"]; //Error

let arrayAny: Array<any> = [0, "one", 2, 3, true];
console.log(`Array any: ${arrayAny}`); //Not recommended

// Tuple
let tuple: [number, string, boolean] = [10, "hello", true];
console.log(`Tuple: ${tuple}`);

let arrayTuple: [string, number][] = [
  ["Rick", 35],
  ["Karl", 20],
];
console.log(`Array tuple: ${arrayTuple}`);

// Inference types
let varInfe;
let varString: string;
let varInference = "Hello";

// Composition types or
let composition: string | number;
composition = "hello";
composition = 30;
console.log(`Composition or: ${composition}`);
