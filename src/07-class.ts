// Class

// Time compilation and execution
// Implements method
// Instance

class Car {
  // default public, private and protected
  protected id!: number;
  private color!: string;
  private model!: number;

  constructor(color: string, model: number) {
    this.color = color;
    this.model = model;
  }

  setId(id: number) {
    return (this.id = id);
  }

  getId() {
    return this.id;
  }

  getColor() {
    return this.color;
  }

  getModel() {
    return this.model;
  }
}

let car = new Car("red", 2000);
console.log(car);

// -----------------------------------
class Vehicle {
  constructor(protected type: string) {}
}

class Bike extends Vehicle {
  constructor(
    private readonly id: number,
    type: string,
    private color: string,
    private model?: number
  ) {
    super(type);
  }

  getColor() {
    return this.color;
  }

  getModel() {
    return this.model;
  }

  showType() {
    return this.type;
  }
}

const bike = new Bike(1, "Bicicle", "blue", 2020);
console.log(bike);
console.log(`Show type: ${bike.showType()}`);
