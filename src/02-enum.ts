// Enums

enum Role {
  admin,
  user,
}

console.log(`Enum `, Role.admin);

enum Roles {
  admin = "ADMIN",
  user = "USER",
}

console.log(`Enum `, Roles.admin);
