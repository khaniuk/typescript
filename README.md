# typescript

## Description

Exercise of TypeScript

## Getting started

Install Nodejs [Official site](https://nodejs.org/en/).

Check node and npm or yarn version

```
node -v

npm -v

yarn -v
```

## Install TypeScript

```
npm install -g typescript

yarn add --global typescript
```

check version

```
tsc -v
```

## TypeScript init and setting

```
"rootDir": "./src"

"outDir": "./dist"
```

Run typescript files

```
tsc
```

Run and test example.js

```
tsc && node dist/example.js
```

## License

For open source projects, [MIT](https://choosealicense.com/licenses/mit/)
